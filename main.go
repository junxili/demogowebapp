package main

import (
	"fmt"
	"html/template"
	"io"
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/", index_handler)
	mux.HandleFunc("/login", login_handler)
	fmt.Printf("Starting server at port 8080\n")
	http.ListenAndServe(":8080", mux)
}

func index_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url=%s\n", r.URL.String())
	io.WriteString(w, r.URL.String())
}

func login_handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		t, _ := template.ParseFiles("login.gtpl")
		t.Execute(w, nil)
	} else {
		r.ParseForm()
		// logic part of log in
		fmt.Println("username:", r.Form["username"])
		fmt.Println("password:", r.Form["password"])
	}
}
